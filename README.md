# SimpleJSONFetcher

Easily fetch JSON content in one quick function call.

## Usage

Just include the source files for the language you're using and you're good to go. Check out examples below.

## Examples

### Swift 5
```
SimpleJSONFetcher.getFrom(URL("https://jsonplaceholder.typicode.com/posts/")!) { (data, error) in
    if let json = data as? [[String: Any]] {
        // ...
    }
}
```